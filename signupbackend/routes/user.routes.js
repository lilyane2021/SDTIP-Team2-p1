const { authJwt } = require("../middlewares");
const controller = require("../controller/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  app.get("/api/test/cus", [authJwt.verifyToken], controller.customerBoard);

  app.get(
    "/api/test/part",
    [authJwt.verifyToken, authJwt.isPartner],
    controller.partnerBoard
  );

  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
};