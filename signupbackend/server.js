const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");
const express = require("express");

const bodyParser = require("body-parser");
const db = require("./config/keys").mongoURI;
dotenv.config({ path: "./config.env" });
const dbb = require("./models");
const Role = dbb.role;
const apps = require("./app");
const app = express();

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Allow-Methods", [
    "PATCH",
    "POST",
    "GET",
    "DELETE",
    "PUT",
  ]);
  res.setHeader("Access-Control-Allow-Headers", ["Content-Type"]);
  res.setHeader("Access-Control-Expose-Headers", ["Content-Type"]);
  next();
});

// mongoose.connect(process.env.DATABASE_ACCESS, () => {
//   console.log("Database connected");
// });
var corsOptions = {
  origin: "http://localhost:3000",
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.get("/", (req, res) => {
  res.json({ message: "Welcome to kidzie application." });
});
console.log("in server.js");

mongoose
  .connect(
    process.env.DATABASE_ACCESS
    //    {
    //   useNewUrlParser: true,
    //   useCreatedIndex: true,
    //   useFindAndModify: false,
    // }
  )
  .then((con) => {
    //console.log(con.connections);
    console.log("DB connected!");
    initial();
  })
  .catch((err) => console.log("here error", err));
function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "customer",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'customer' to roles collection");
      });

      new Role({
        name: "partner",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'partner' to roles collection");
      });

      new Role({
        name: "admin",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });
    }
  });
}
require("./routes/auth.routes")(app);
require("./routes/user.routes")(app);
const salesRoute = require("./routes/salesRoute");
app.use("/app/v1/sales", salesRoute);
// require("./routes/salesRoute.js")(app);
const PORT = 4230;
app.listen(PORT, () => console.log(`server is up and running on port ${PORT}`));
