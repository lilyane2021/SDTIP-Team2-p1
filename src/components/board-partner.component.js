import React, { Component } from "react";

import UserService from "../services/user.service";
import CouponsAdd from './CouponsAdd'
import CouponsList from '../components/CouponsList'
export default class BoardPartner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  

  render() {
    return (
       <div>
        <CouponsList />
        <CouponsAdd />
        </div>
      
    );
  }
}