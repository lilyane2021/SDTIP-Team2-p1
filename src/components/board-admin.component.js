import React, { Component } from "react";
import logo from './Kidzie Logo 3.png'; 
import UserService from "../services/user.service";
import './Sign up Style.css'
export default class BoardAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  

  render() {
    return (
  <div>
  
  <meta name="viewport" content="width=device-width, initial-scale=1" />
 
  <p style={{position: 'absolute', top: 8, left: 16}}>Home /Employee account /Profile</p>
  <div className="row">
    <div className="column1 left" style={{backgroundColor: '#F5F5DC'}}>
      <a href="Home.html">
        <img data-v-0df74896 id="kidzieLogo" src={logo} text-align="left top" style={{width: '8.5%'}} />
      </a>
    </div>
    <div className="column2 left" style={{backgroundColor: '#be9428'}}>
    </div>
  </div>
  <p className="form"> General Information</p>
  <br />
  <form action="#" method="POST" className="form1">
    <br />
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; First Name<span className="required">*</span></label>
    <input type="text" style={{backgroundColor: '#F5F5DC'}} pattern="[A-Za-z]+" name="UserName" required />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; Phone<span className="required">*</span></label>
    <input type="tel" style={{backgroundColor: '#F5F5DC'}} pattern="[0-9]{11}" name="PhoneNumber" required /><br /><br />
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; Last Name<span className="required">*</span></label>
    <input type="text" style={{backgroundColor: '#F5F5DC'}} pattern="[A-Za-z]+" name="UserName" required />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; Gender<span className="required">*</span></label>
    <select name="Gender" style={{backgroundColor: '#F5F5DC'}}>
      <option value>Please Select</option>
      <option value={1}>Male</option>
      <option value={0}>Female</option>
    </select><br /><br /> 
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; Email<span className="required">*</span></label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="email" style={{backgroundColor: '#F5F5DC'}} name="Email" required />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label className="label">  Country<span className="required">*</span></label>
    <select name="country" style={{backgroundColor: '#F5F5DC'}}>
      <option value>Select a Country</option>
      <option>USA</option>
      <option>Egypt</option>
      <option>KSA</option>
      <option>Lebanon</option>
      <option>UK</option>
    </select><br /><br /> 
    <input className="button button1 button3" type="submit" defaultValue="Save" /><br /><br /><br /><br />
    <input className="button button1 button2" type="submit" defaultValue="Cancel" />
  </form>
  <p className="form"> Account Information</p>
  <br />
  <form action="#" method="POST" className="form1">
    <br />
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; User Name<span className="required">*</span></label>
    <input type="text" style={{backgroundColor: '#F5F5DC'}} pattern="[A-Za-z]+" name="UserName" required />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; Old Password<span className="required">*</span></label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="password" style={{backgroundColor: '#F5F5DC'}} name="Password" required /><br /><br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; New Password<span className="required">*</span></label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="password" style={{backgroundColor: '#F5F5DC'}} name="Password" required /><br /><br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label className="label"> &nbsp;&nbsp;&nbsp;&nbsp; Confirm Password<span className="required">*</span></label>
    <input type="password" style={{backgroundColor: '#F5F5DC'}} name="Password" required /><br /><br />
    <input className="button button1 button4" type="submit" defaultValue="Save" /><br /><br /><br /><br />
    <input className="button button1 button2" type="submit" defaultValue="Cancel" />
  </form>
  <br /> <br />
  <div className="sidenav">
    <a href="Home.html">Home</a>
    <a href="#">Profile</a>
    <a href="#">Kidzie Catalouge</a>
    <a href="#">Inventory Management</a>
    <a href="#" className="active">Orders</a>
    <a href="#" className="active">Reports</a>
    <a href="#" className="active">Products</a>
    <a href="#" className="active">Returns</a>
    <a href="#" className="active">Shipments</a>
    <a href="#" className="active">Statements</a>
    <a href="#" className="active">Partners</a>
    <a href="#" className="active">Teams</a>
    <a href="#" className="active">Communications</a>
    <a href="#" className="active">Logout</a>
  </div>
  <div className="footer">
    <p> &nbsp;&nbsp; Kidzie International</p>
    <div className="social-media">
      <a href="https://www.facebook.com/kidzie.ca/" className="fa fa-facebook" />
      <a href="https://twitter.com/kidziedotcom?lang=en" className="fa fa-twitter" />
      <a href="https://www.linkedin.com/company/kidzie-international/?originalSubdomain=ca" className="fa fa-linkedin" />
      <a href="https://www.instagram.com/kidzie.ca/?hl=en" className="fa fa-instagram" />
    </div>
  </div> 
</div>


    );
  }
}