import React, { Component } from "react";
import AuthService from "../services/auth.service";
import './Home Style.css';
import logo from './Kidzie Logo 3.png';
import last from './last.png'
export default class Profile extends Component {

  render() {
    

    return (
      
     <div>
       

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  
  <div className="row">
    <div className="column1" style={{backgroundColor: '#F5F5DC'}}>
      <a href="Home.html">
        <img data-v-0df74896 id="kidzieLogo" src={logo} text-align="left top" style={{width: '8.5%'}} />
      </a>
    </div>
    <div className="column2" style={{backgroundColor: '#be9428'}}>
    </div>
  </div>
  <br /><br /><br /><br /><br /><br /><br />
  <p className="welcome"> Welcome to Kidzie</p>
  <img data-v-0df74896 id="kidzieLogo" src={last} text-align="left top" style={{width: '100%'}} />
  
  <div className="footer">
    
    <div className="social-media">
      <a href="https://www.facebook.com/kidzie.ca/" className="fa fa-facebook" />
      <a href="https://twitter.com/kidziedotcom?lang=en" className="fa fa-twitter" />
      <a href="https://www.linkedin.com/company/kidzie-international/?originalSubdomain=ca" className="fa fa-linkedin" />
      <a href="https://www.instagram.com/kidzie.ca/?hl=en" className="fa fa-instagram" />
    </div>
  </div> 
</div>

    );
  }
}