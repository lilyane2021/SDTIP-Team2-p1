import React, { Component } from "react";

import UserService from "../services/user.service";
import FilterSearch from "./FilterSearch";
import ProductComp from "./ProductComp";
import SalesAdding from "./SalesAdding";
export default class BoardSalesPartner extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
    };
  }

  render() {
    return (
      <div>
        <FilterSearch />
        <ProductComp />
        <SalesAdding />
      </div>
    );
  }
}
