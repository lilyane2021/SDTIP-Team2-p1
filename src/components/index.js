export { default as Navigation } from "./Navigation";
export { default as Footer } from "./Footer";
export { default as Home } from "./Home";
export { default as About } from "./About";
export { default as Contact } from "./Contact";
export { default as AppOne } from "./AppOne";
export { default as SalesCouponsPage } from "./SalesCoupnsPage";
export { default as Signin } from "./Signin";
export { default as Register} from "./register.component";
export { default as Login } from "./login.component";

export { default as Profile} from "./profile.component";