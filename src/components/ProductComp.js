import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { FaEdit, FaTrash } from "react-icons/fa";
import axios from "axios";
import "./html/SalesPartnerPageStyle.css";
import imglogo3 from "./html/KidzieLogo3.png";
import { useGlobalContext } from "../Context";

export default function ProductComp() {
  //copied from CouponsList
  const {
    isEditing,
    setIsEditing,
    couponsList,
    setcouponsList,
    salesList,
    setsalesList,
    editID,
    setEditID,
    setAlert,
    isAdding,
    setIsAdding,
  } = useGlobalContext();
  const salesListAsync = async () => {
    const salesList = await axios.get(
      "http://localhost:4230/app/v1/sales/getAllProduct"
    );
    console.log(salesList.data.data);
    //return salesList.data.data;
  };

  useEffect(() => {
    //salesListAsync().then((resp) => setsalesList(resp));
  }, []);

  const tableData = salesList.map((sale) => {
    const { _id } = sale; //status, code, startDate, endDate, percentage
    console.log(sale);

    return (
      <tr key={_id}>
        <td>
          <b>test</b>
        </td>
        <td>test</td>
        <td>test</td>
        <td>test</td>
        <td>test</td>
        <td>test</td>
        <td>test</td>

        {/* <td>
          <div className="btn-container">
            <button
              type="button"
              className="edit-btn"
              onClick={() => }
            >
              <FaEdit />
            </button>
            <button
              type="button"
              className="delete-btn"
              onClick={() => removeItem(_id)}
            >
              <FaTrash />
            </button>
          </div>
        </td> */}
      </tr>
    );
  });

  return (
    <div style={{ backgroundColor: "#F5F5DC;" }}>
      hello from sales product
      <div class="row">
        <div class="column1 " style={{ backgroundColor: "#F5F5DC;" }}>
          <a href="Home.html">
            <img
              data-v-0df74896=""
              id="kidzieLogo"
              src={imglogo3}
              style={{ width: "2.5%;", height: "20%", textAlign: "left top" }}
            />
          </a>
        </div>

        <div class="column2 left" style={{ backgroundColor: "#be9428" }}></div>
      </div>
      <p class="form"> Promo Codes </p>
      <form onSubmit={() => console.log("submit")}>
        <div style={{ overflowX: "auto;", position: "relative; left: 200px;" }}>
          <table>
            <tr>
              <th>Code</th>
              <th>Status</th>
              <th>Starts</th>
              <th>Ends</th>
              <th>Percentage</th>
              <th>Emails</th>
              <th>Criteria </th>
              {/* <th>Action </th> */}
            </tr>
            <tbody>{tableData}</tbody>
          </table>
        </div>
        <br />
        <br />
        <br />
        <button
          type="submit"
          className="button button1 button3"
          style={{ position: "relative; left: 1150px" }}
        >
          {isEditing ? "Edit Sale" : "Add Sale"}
        </button>

        <br />
        <br />
        <br />
      </form>
    </div>
  );
}
