import React, { Component } from "react";
import './Customer Order Style.css'
import UserService from "../services/user.service";
import cart from './cart icon 5.png';

export default class BoardCustomer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  

  render() {
    return (
      <div>
  
  
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
  <div className="row">
  
    <div className="column2 left" style={{backgroundColor: '#be9428'}}>
    </div>
  </div>
  <form action="Adding Promo Code.html">
    <div style={{overflowX: 'auto', position: 'relative', left: 200}}>
      <br /><table>
        <tbody><tr>
            <th>Orders</th>
          </tr>
          <tr>
            <td><img src={cart}  alt height={100} width={100} /></td>
            <td>January 20,2019</td>
            <td>293744577</td>
            <td>Open</td>
            <td>12 Products</td>
            <td>$503.90</td>
            <td><button className="btn add btn-info" style={{backgroundColor: '#be9428'}} /></td>
          </tr>
          <tr>
            <td><img src={cart} alt height={100} width={100} /></td>
            <td>January 23,2019</td>
            <td>836875539</td>
            <td>Open</td>
            <td>5 Products</td>
            <td>$220.00</td>
            <td><button className="btn add btn-info" style={{backgroundColor: '#be9428'}} /></td>
          </tr>
          <tr>
            <td><img src={cart}  alt height={100} width={100} /></td>
            <td>January 28,2019</td>
            <td>103332739</td>
            <td>Closed</td>
            <td>2 Products</td>
            <td>$1205.30</td>
            <td><button className="btn add btn-info" style={{backgroundColor: '#be9428'}} /></td>
          </tr>
          <tr>
            <td><img src={cart}  alt height={100} width={100} /></td>
            <td>Februray 5,2019</td>
            <td>119446253</td>
            <td>Closed</td>
            <td>23 Products</td>
            <td>$840.10</td>
            <td><button className="btn add btn-info" style={{backgroundColor: '#be9428'}} /></td>
          </tr>
        </tbody></table>
    </div>
    <br /><br /><br />
    <br /><br /><br />
  </form>
  <div className="sidenav">
    <a href="Customer Order.html">Orders</a>
    <a href="Profile Customer.html">Profile</a>
    <a href="#">Preferences</a>
    <a href="Sign in.html" className="active">Logout</a>
  </div>
  <div className="footer">
    <p> &nbsp;&nbsp; Kidzie International</p>
    <div className="social-media">
      <a href="https://www.facebook.com/kidzie.ca/" className="fa fa-facebook" />
      <a href="https://twitter.com/kidziedotcom?lang=en" className="fa fa-twitter" />
      <a href="https://www.linkedin.com/company/kidzie-international/?originalSubdomain=ca" className="fa fa-linkedin" />
      <a href="https://www.instagram.com/kidzie.ca/?hl=en" className="fa fa-instagram" />
    </div>
  </div> 
</div>

    );
  }
}