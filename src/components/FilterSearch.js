import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { FaEdit, FaTrash } from "react-icons/fa";
import axios from "axios";
import "./html/SalesPartnerPageStyle.css";
import imglogo3 from "./html/KidzieLogo3.png";
import { useGlobalContext } from "../Context";

export default function FilterSearch() {
  //copied from CouponsList
  const [state, setState] = useState({
    kidzieId: "",
    prodGrp: "",
    ageGrp: "",
    gender: "",
    percentage: "",
    productGroup: "",
    designer: "",
  });
  const handleChange = (e) => {
    const name = e.target.name;
    console.log(name);
    const value = e.target.value;
    console.log(value);
    setState({ ...state, [name]: value });
  };
  const {
    isEditing,
    setIsEditing,
    couponsList,
    setcouponsList,
    editID,
    setEditID,
    setAlert,
    isAdding,
    setIsAdding,
  } = useGlobalContext();

  return (
    <div>
      hello from FilterSearch Component
      <div class="row">
        <div class="column1 left" style={{ backgroundColor: "#F5F5DC;" }}>
          <a href="Home.html">
            <img
              data-v-0df74896=""
              id="kidzieLogo"
              src={imglogo3}
              text-align="left top"
              style={{ width: "8.5%;" }}
            />
          </a>
        </div>
        <div class="column2 left" style={{ backgroundColor: "#be9428" }}></div>
      </div>
      <p class="form"> Sales </p>
      <br />
      <form
        action="#"
        method="POST"
        class="form1"
        style={{ backgroundColor: "#F5F5DC;" }}
      >
        <br />
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp; Kidzie ID
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <span class="required">*</span>
        </label>
        <input
          type="number"
          style={{ backgroundColor: "#F5F5DC;" }}
          pattern="[0-9]"
          name="ID"
          required
        />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp; Products added{" "}
          <span class="required">*</span>
        </label>
        <input
          placeholder="From"
          style={{ backgroundColor: "#F5F5DC;", width: "180px;" }}
          class="textbox-n"
          type="date"
          onfocus="(this.type='date')"
          id="date"
        />
        <input
          placeholder="To"
          style={{ backgroundColor: "#F5F5DC;", width: "180px;" }}
          class="textbox-n"
          type="date"
          onfocus="(this.type='date')"
          id="date"
        />
        <br />
        <br />
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp; Prouduct Group{" "}
          <span class="required">*</span>
        </label>
        <select name="product group" style={{ backgroundColor: "#F5F5DC;" }}>
          <option value="">Please Select</option>
          <option value="1">-</option>
          <option value="2">-</option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp; On Sale?
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <span class="required">*</span>
        </label>
        <select name="on sale" style={{ backgroundColor: "#F5F5DC;" }}>
          <option value="">Please Select</option>
          <option value="1">Yes</option>
          <option value="2">No</option>
        </select>
        <br />
        <br /> &nbsp;&nbsp;&nbsp;&nbsp;
        <label class="label">
          {" "}
          Age Group &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <span class="required">*</span>
        </label>
        <select name="age group" style={{ backgroundColor: "#F5F5DC;" }}>
          <option value="">Please Select</option>
          <option value="1">-</option>
          <option value="2">-</option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;
        <label class="label">
          {" "}
          Sale %
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <span class="required">*</span>
        </label>
        <select name="sale" style={{ backgroundColor: "#F5F5DC;" }}>
          <option value="">Please Select</option>
          <option value="1">5%</option>
          <option value="2">10%</option>
          <option value="3">15%</option>
          <option value="4">20%</option>
          <option value="5">25%</option>
          <option value="6">30%</option>
          <option value="7">35%</option>
          <option value="8">40%</option>
          <option value="9">45%</option>
          <option value="10">50%</option>
          <option value="11">55%</option>
          <option value="12">60%</option>
          <option value="13">65%</option>
          <option value="14">70%</option>
          <option value="15">75%</option>
          <option value="16">80%</option>
        </select>
        <br></br>
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp;
          Gender&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <span class="required">*</span>
        </label>
        <select name="Gender" style={{ backgroundColor: "#F5F5DC;" }}>
          <option value="">Please Select</option>
          <option value="1">Male</option>
          <option value="0">Female</option>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp; Designer
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
          <span class="required">*</span>
        </label>
        <select name="designer" style={{ backgroundColor: "#F5F5DC;" }}>
          <option value="">Please Select</option>
          <option value="1">-</option>
          <option value="2">-</option>
        </select>
        <br />
        <br />
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp; Categories
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{" "}
          <span class="required">*</span>
        </label>
        <div id="list1" class="dropdown-check-list" tabindex="100">
          <span class="anchor">Please Select</span>
          <ul class="items">
            <li>
              <input
                type="checkbox"
                style={{ position: "relative;", right: "110px;" }}
              />
              Shirts{" "}
            </li>
            <li>
              <input
                type="checkbox"
                style={{ position: "relative;", right: "110px;" }}
              />
              Polos
            </li>
            <li>
              <input
                type="checkbox"
                style={{ position: "relative;", right: "110px;" }}
              />
              T-Shirts
            </li>
            <li>
              <input
                type="checkbox"
                style={{ position: "relative;", right: "110px;" }}
              />
              Graphics
            </li>
            <li>
              <input
                type="checkbox"
                style={{ position: "relative;", right: "110px;" }}
              />
              Blouses
            </li>
          </ul>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <label class="label">
          {" "}
          &nbsp;&nbsp;&nbsp;&nbsp; General Search{" "}
          <span class="required">*</span>
        </label>
        <input
          type="text"
          style={{ backgroundColor: "#F5F5DC;" }}
          pattern="[A-Za-z0-9]+"
          name="general search"
          required
        />
        <br />
        <br />
        <br />
        <br />
        <br />
        <input class="button button1 button3" type="submit" value="Search" />
        <br />
        <br />
        <br />
        <br />
      </form>
      <p class="form2"> </p>
      <br />
      <br />
      <br />
    </div>
  );
}
