import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import {
  Navigation,
  Footer,
  Home,
  About,
  Contact,
  AppOne,
  SalesCouponsPage,
  Signin,
} from "./components";
import "bootstrap/dist/css/bootstrap.min.css";
import Profile from "./components/profile.component";

import "./App.css";

import AuthService from "./services/auth.service";
import BoardCustomer from "./components/board-customer.component";
import BoardPartner from "./components/board-partner.component";
import BoardSalesPartner from "./components/board-sales-partner.component";
import BoardAdmin from "./components/board-admin.component";
import Login from "./components/login.component";
import Register from "./components/register.component";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showPartnerBoard: false,
      showAdminBoard: false,
      showCustomerBoard: false,
      currentUser: undefined,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
        showPartnerBoard: user.roles.includes("ROLE_PARTNER"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
        showCustomerBoard: user.roles.includes("ROLE_CUSTOMER"),
      });
    }
  }

  render() {
    const { currentUser, showPartnerBoard, showAdminBoard, showCustomerBoard } =
      this.state;

    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/home"} className="nav-link">
                Home
              </Link>
            </li>
            {showPartnerBoard && (
              <>
                <li className="nav-item">
                  <Link to={"/part"} className="nav-link">
                    coupons
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to={"/salespage"} className="nav-link">
                    Sales
                  </Link>
                </li>
              </>
            )}

            {showAdminBoard && (
              <>
                <li className="nav-item">
                  <Link to={"/admin"} className="nav-link">
                    Admin Board
                  </Link>
                </li>
              </>
            )}
            {showCustomerBoard && (
              <li className="nav-item">
                <Link to={"/customer"} className="nav-link">
                  Customer Board
                </Link>
              </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Login
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Sign Up
                </Link>
              </li>
            </div>
          )}
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/home"]} component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route path="/part" component={BoardPartner} />
            <Route path="/salespage" component={BoardSalesPartner} />
            <Route path="/admin" component={BoardAdmin} />
            <Route path="/customer" component={BoardCustomer} />
            {/* <Navigation />
            <Switch>
              <Route path="/appone">
                <AppOne />
              </Route>
              <Route path="/salesCoupons">
                <SalesCouponsPage />
              </Route>
            </Switch> */}
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
